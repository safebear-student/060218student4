package com.safebear.app;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.FramesPage;
import pages.LoginPage;
import pages.UserPage;
import pages.WelcomePage;

import java.util.concurrent.TimeUnit;

/**
 * Created by cca_student on 06/02/2018.
 */
public class BaseTest {
    WebDriver driver;
    WelcomePage welcomePage;
    LoginPage loginPage;
    UserPage userPage;
    FramesPage framesPage;

    @Before
    public void setUp(){
        driver = new ChromeDriver();
        welcomePage = new WelcomePage(driver);
        loginPage = new LoginPage(driver);
        userPage = new UserPage(driver);
        framesPage = new FramesPage(driver);
        driver.get("http://automate.safebear.co.uk/");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @After
    public void tearDown(){
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        driver.quit();
    }

}
