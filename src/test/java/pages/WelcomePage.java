package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by cca_student on 06/02/2018.
 */
public class WelcomePage {
    WebDriver driver;

    @FindBy(linkText = "Login")
    WebElement loginLink;

    public WelcomePage(WebDriver _driver) {
        this.driver = _driver;
        PageFactory.initElements(driver, this);
    }

    public boolean checkCorrectPage() {
        return driver.getTitle().startsWith("Welcome");
    }

    public boolean clickOnLogin(LoginPage _loginPage) {
        loginLink.click();
        return _loginPage.checkCorrectPage();
    }

}
